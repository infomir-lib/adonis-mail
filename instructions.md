## Registering an instance

```js
const Mail = require('adonis-mail')
const View = use('Adonis/Src/View')
```

That's all! Now you can use the mail as follows.

```js
const connector = {
                        '_id': '5c7e4e74575f57174be02ba1',
                        'name': 'SMTP connector',
                        'active': true,
                        'config': {
                          'connection': 'smtp',
                          'smtp': {
                            'driver': 'smtp',
                            'port': 587,
                            'host': 'mail.domain.com',
                            'secure': false,
                            'auth': {
                              'user': 'user@domain.com',
                              'pass': 'userpwd'
                            },
                            'tls': {
                              'rejectUnauthorized': false
                            },
                            'maxConnections': 5,
                            'maxMessages': 100,
                            'rateLimit': 10
                          }
                        }
                      }

const mail = new Mail(connector, View)

let response = ''
    try {
      const mail = new Mail(connector, View)
      responseOk = await mail.send('emails.alert', connector.toJSON(), (message) => {
        message
          .to('d.munjin@infomir.com')
          .from('postmaster@smart4city.ch')
          .subject('Alerts from CMS')
      })
    } catch (e) {
      console.log(e)
    }
    console.log(JSON.stringify(response, null, 2))
```

The `alert` is the view name stored inside the `resources/views/emails` directory.
